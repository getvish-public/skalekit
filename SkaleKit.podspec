Pod::Spec.new do |s|
  s.name             = "SkaleKit"
  s.version          = "0.5.0"
  s.summary          = "SkaleKit is a iOS api for accessing Skale electric scale."
  s.homepage         = "https://github.com/atomaxinc/SkaleKit"
  s.license          = 'MIT'
  s.author           = { "Ryan" => "ryan@atomaxinc.com" }
  s.source           = { :git => "git@professor-frink.getvish.com:ryne/skalekit.git" }
  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.frameworks = 'UIKit', 'CoreBluetooth'
  s.ios.vendored_frameworks = 'SkaleKit.framework'
end
